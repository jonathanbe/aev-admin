import PartnerList from './PartnerList';
import PartnerForm from './PartnerForm';

export default [
	{
		path: '/partners',
		component: PartnerList,
		meta: { requiresAuth: true }
	},
	{
		path: '/partners/add',
		component: PartnerForm,
		meta: { requiresAuth: true }
	},
	{
		path: '/partners/edit/:id',
		component: PartnerForm,
		meta: { requiresAuth: true }
	}
]
