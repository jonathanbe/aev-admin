import CategoryList from './CategoryList';
import CategoryForm from './CategoryForm';

export default [
	{
		path: '/categories',
		component: CategoryList,
		meta: { requiresAuth: true }
	},
	{
		path: '/categories/add',
		component: CategoryForm,
		meta: { requiresAuth: true }
	},
	{
		path: '/categories/edit/:id',
		component: CategoryForm,
		meta: { requiresAuth: true }
	}
]
