import DonationList from './DonationList';
import DonationDialog from './DonationDialog';

export default [
	{
		path: '/donations',
		component: DonationList,
		meta: { requiresAuth: true },
		children: [
			{
				path: ':id',
				component: DonationDialog
			}
		]
	},
]
