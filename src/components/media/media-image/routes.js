import MediaImageList from './MediaImageList';

export default [
	{
		path: '/images',
		component: MediaImageList,
		meta: { requiresAuth: true }
	}
]
