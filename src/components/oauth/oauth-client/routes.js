import OAuthClientList from './OAuthClientList';
import OAuthClientForm from './OAuthClientForm';

export default [
	{
		path: '/oauth/clients',
		component: OAuthClientList,
		meta: { requiresAuth: true }
	},
	{
		path: '/oauth/clients/edit/:id',
		component: OAuthClientForm,
		meta: { requiresAuth: true }
	},
	{
		path: '/oauth/clients/add',
		component: OAuthClientForm,
		meta: { requiresAuth: true }
	}
]
