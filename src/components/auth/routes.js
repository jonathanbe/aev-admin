import Auth from './Auth';
import AuthLogin from './AuthLogin';
import AuthForgot from './AuthForgot';
import AuthReset from './AuthReset';

export default [
	{
		path: '/auth',
		component: Auth,
		children: [
			{
				path: '/',
				redirect: 'login'
			},
			{
				path: 'login',
				component: AuthLogin,
			},
			{
				path: 'forgot',
				component: AuthForgot
			},
			{
				path: 'reset/:email/:token',
				component: AuthReset
			}
		]
	}
]
