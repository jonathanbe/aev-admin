import VolunteerList from './VolunteerList';
import VolunteerForm from './VolunteerForm';

export default [
	{
		path: '/volunteers',
		component: VolunteerList,
		meta: { requiresAuth: true }
	},
	{
		path: '/volunteers/add',
		component: VolunteerForm,
		meta: { requiresAuth: true }
	},
	{
		path: '/volunteers/edit/:id',
		component: VolunteerForm,
		meta: { requiresAuth: true }
	}
]
