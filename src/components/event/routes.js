import EventList from './EventList';
import EventForm from './EventForm';

export default [
	{
		path: '/events',
		component: EventList,
		meta: { requiresAuth: true }
	},
	{
		path: '/events/add',
		component: EventForm,
		meta: { requiresAuth: true }
	},
	{
		path: '/events/edit/:id',
		component: EventForm,
		meta: { requiresAuth: true }
	}
]
