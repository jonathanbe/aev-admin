import UserList from './UserList';
import UserForm from './UserForm';

export default [
	{
		path: '/users',
		component: UserList,
		meta: { requiresAuth: true }
	},
	{
		path: '/users/add',
		component: UserForm,
		meta: { requiresAuth: true }
	},
	{
		path: '/users/edit/:id',
		component: UserForm,
		meta: { requiresAuth: true }
	}
]
