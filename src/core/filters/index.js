import Capitalize from './capitalize';

export default function install(Vue) {
	Vue.filter('capitalize', Capitalize);
}
