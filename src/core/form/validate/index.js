import VeeValidate from 'vee-validate';
import pt_BR from 'vee-validate/dist/locale/pt_BR';

export default function install(Vue) {
	Vue.use(VeeValidate, { locale: 'pt_BR', dictionary: { pt_BR } });
}
