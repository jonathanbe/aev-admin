export default {
	created () {
		// Subscribes to parent's validate
		this.$root.$on('validate', this.onValidate);
	},
	methods: {
		onValidate () {
			this.$validator.validateAll()
				.then(success => {
					this.$root.$emit('errors-changed', success);
				});
		}
	},
	beforeDestroy () {
		this.$root.$off('validate', this.onValidate);
	}
}
