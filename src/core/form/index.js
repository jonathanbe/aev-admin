import Validate from './validate';

export validateChild from './validate/child';
export validateParent from './validate/parent';

export default function install(Vue) {
	Vue.use(Validate);
}
