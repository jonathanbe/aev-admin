import SearchInput from './search/SearchInput';
import ModelStatus from './model/ModelStatus';

export default function install(Vue) {
	Vue.component('adm-search-input', SearchInput)
	Vue.component('adm-model-status', ModelStatus)
}
