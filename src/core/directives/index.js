import MomentAgo from './moment/ago';
import MaskPhone from './mask/phone';
import MaskDate from './mask/date';
import MaskCPF from './mask/cpf';
import DatePicker from './date/picker';

export default function install(Vue) {
	Vue.directive('moment-ago', MomentAgo);
	Vue.directive('mask-phone', MaskPhone);
	Vue.directive('mask-date', MaskDate);
	Vue.directive('mask-cpf', MaskCPF);
	Vue.directive('date-picker', DatePicker);
}
