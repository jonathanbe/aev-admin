import VMasker from 'vanilla-masker';

export default {
	inserted (el, binding) {
		VMasker(el).maskPattern('99/99/9999');
	},

	unbind (el) {
		//
	}
}
