import VMasker from 'vanilla-masker';

export default {
	inserted (el, binding) {
		VMasker(el).maskPattern('999.999.999-99');
	},

	unbind (el) {
		//
	}
}
