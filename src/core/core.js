import VueRouter from 'vue-router';
import VueMushi from 'vue-mushi';

import Stylesheet from './stylesheet';
import Form from './form';
import Http from './http';
import Filters from './filters';
import Shared from './shared';
import Directives from './directives';

export default function install(Vue) {
	if (install.installed) {
		console.warn('Admin core is already installed.');
		return;
	}

	install.installed = true;

	Vue.use(VueRouter);
	Vue.use(VueMushi);
	Vue.use(Stylesheet);
	Vue.use(Form);
	Vue.use(Http);
	Vue.use(Shared);
	Vue.use(Filters);
	Vue.use(Directives);
}
