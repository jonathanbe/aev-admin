import VueRouter from 'vue-router';
import routes from './routes';

let router = new VueRouter({
	base: process.env.BASE_PATH,
	mode: 'history',
	routes
});

// Execute router guards
require('./guard')(router)

export default router;
