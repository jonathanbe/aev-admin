import authRoutes from '../../components/auth/routes';
import dashboardRoutes from '../../components/dashboard/routes';
import userRoutes from '../../components/user/routes';
import pageRoutes from '../../components/page/routes';
import postRoutes from '../../components/post/routes';
import categoryRoutes from '../../components/category/routes';
import mediaImageRoutes from '../../components/media/media-image/routes';
import eventRoutes from '../../components/event/routes';
import partnerRoutes from '../../components/partner/routes';
import volunteerRoutes from '../../components/volunteer/routes';
import donationRoutes from '../../components/donation/routes';
import institutionRoutes from '../../components/institution/routes';
import OAuthRoutes from '../../components/oauth/routes';

export default [
	...authRoutes,
	...dashboardRoutes,
	...userRoutes,
	...pageRoutes,
	...postRoutes,
	...categoryRoutes,
	...mediaImageRoutes,
	...eventRoutes,
	...partnerRoutes,
	...volunteerRoutes,
	...donationRoutes,
	...institutionRoutes,
	...OAuthRoutes,
	{ path: '*', redirect: 'auth' },
];
