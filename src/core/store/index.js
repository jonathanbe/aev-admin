import Vue from 'vue';
import Vuex from 'vuex';

import oauth from './modules/oauth';
import auth from './modules/auth';
import sidenav from './modules/sidenav';
import user from './modules/user';
import password from './modules/password';
import page from './modules/page';
import post from './modules/post';
import location from './modules/location';
import category from './modules/category';
import media from './modules/media';
import event from './modules/event';
import institution from './modules/institution';
import partner from './modules/partner';
import volunteer from './modules/volunteer';
import donation from './modules/donation';

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		auth,
		oauth,
		sidenav,
		user,
		password,
		page,
		post,
		location,
		category,
		media,
		event,
		institution,
		partner,
		volunteer,
        donation
	}
})
