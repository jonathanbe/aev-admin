import Vue from 'vue';

import { getHeaders } from '../../';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getCategories (context, payload) {
			return Vue.http.get(process.env.API_URL + '/categories',
				{ headers: getHeaders() }
			);
		},
		addCategory (context, payload) {
			return Vue.http.post(process.env.API_URL + '/categories',
				payload,
				{ headers: getHeaders() }
			)
		},
		getCategory (context, payload) {
			return Vue.http.get(process.env.API_URL + '/categories/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		editCategory (context, payload) {
			return Vue.http.put(process.env.API_URL + '/categories/' + payload.page.id,
				payload,
				{ headers: getHeaders() }
			);
		},
		deleteCategory (context, payload) {
			return Vue.http.delete(process.env.API_URL + '/categories/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		nextCategory(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
