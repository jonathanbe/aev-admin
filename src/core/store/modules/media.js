import Vue from 'vue';

import { getHeaders } from '../../';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getImages (context, payload) {
			return Vue.http.get(process.env.API_URL + '/medias/images',
				{ headers: getHeaders() }
			);
		},
		getFiles (context, payload) {
			return Vue.http.get(process.env.API_URL + '/medias/files',
				{ headers: getHeaders() }
			);
		},
		addMedia (context, payload) {
			let data = new FormData();

			data.append('file', payload.current.file);

			return Vue.http.post(process.env.API_URL + '/medias',
				data,
				{
					headers: getHeaders(),
					progress: e => payload.process({
						lengthComputable: true,
						loaded: (e.loaded / e.total) * 100,
						total: 100
					}),
					before (request) {
						// abort previous request, if exists
						if (Vue.previousRequest) {
							Vue.previousRequest.abort();
						}
						// set previous request on Vue instance
						Vue.previousRequest = request;
					}
				}
			)
		},
		getImage (context, payload) {
			return Vue.http.get(process.env.API_URL + '/images/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		editMedia (context, payload) {
			return Vue.http.put(process.env.API_URL + '/medias/' + payload.media.id,
				payload,
				{ headers: getHeaders() }
			);
		},
		deleteMedia (context, payload) {
			return Vue.http.delete(process.env.API_URL + '/medias/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		nextPage(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
