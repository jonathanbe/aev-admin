import Vue from 'vue';

import { getHeaders } from '../../';

export default {
	namespaced: true,
	state: {
		institutions: []
	},
	actions: {
		fetchInstitutions ({ state }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(process.env.API_URL + '/institutions',
					{ headers: getHeaders() }
				)
				.then(response => {
					if (response.status == 200) {
						state.institutions = response.body.data;

						return resolve(state.institutions);
					}

					return reject(response);
				}, error => console.error(error));
			});
		},
		getInstitution (context, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(process.env.API_URL + '/institutions/' + payload.id,
					{ headers: getHeaders() }
				)
				.then(response => {
					if (response.status == 200) {
						return resolve(response.body);
					}

					return reject(response);
				}, error => console.error(error));
			})
		},
		editInstitution (context, payload) {
			return Vue.http.put(process.env.API_URL + '/institutions/' + payload.institution.id,
				payload,
				{ headers: getHeaders() }
			);
		}
	},
	getters: {
		getInstitutions (state, payload) {
			return state.institutions;
		}
	},
	mutations: {
		//
	}
}
