import Vue from 'vue';

import { getHeaders } from '../../';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getUsers (context, payload) {
			return Vue.http.get(process.env.AUTH_URL + '/users',
				{ headers: getHeaders() }
			);
		},
		searchUsers (context, payload) {
			return Vue.http.get(process.env.AUTH_URL + `/users/search/${payload.q}`,
				{ headers: getHeaders() }
			);
		},
		addUser (context, payload) {
			return Vue.http.post(process.env.AUTH_URL + '/users',
				{
					profile: payload.user.profile,
					username: payload.user.accounts.local.username,
					password: payload.password,
					roles: payload.user.roles
				},
				{ headers: getHeaders() }
			)
		},
		getUser (context, payload) {
			return Vue.http.get(process.env.AUTH_URL + '/users/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		editUser (context, payload) {
			return Vue.http.patch(process.env.AUTH_URL + '/users/' + payload.user._id,
				{
					profile: payload.user.profile,
					username: payload.user.accounts.local.username,
					password: payload.password,
					roles: payload.user.roles
				},
				{ headers: getHeaders() }
			);
		},
		deleteUser (context, payload) {
			return Vue.http.delete(process.env.AUTH_URL + '/users/' + payload._id,
				{ headers: getHeaders() }
			);
		},
		nextPage(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
