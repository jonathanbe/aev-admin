import Vue from 'vue';

import { getHeaders } from '../../';

export default {
	namespaced: true,
	state: {
		user: null,
	},
	getters: {
		getUser(state) {
			return state.user;
		}
	},
	actions: {
		login({ state, commit, dispatch }, payload) {
			return Vue.http.post(`${process.env.AUTH_URL}/auth/login`, {
				username: payload.username,
				password: payload.password
			}, {
				headers: getHeaders()
			})
		},
		userInfo() {
			return Vue.http.get(`${process.env.AUTH_URL}/auth/userinfo`, {
				headers: getHeaders()
			});
		}
	},
	mutations: {
		setUser(state, payload) {
			state.user = payload;
		},
		logout(state, payload) {
			localStorage.removeItem('access_token');
			localStorage.removeItem('refresh_token');
			localStorage.removeItem('expires_at');
			state.user = null;
		}
	}
}
