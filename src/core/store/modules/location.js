import Vue from 'vue';

import { getHeaders } from '../../';

export default {
	namespaced: true,
	state: {
		countries: {},
	},
	actions: {
		getCountries (context, payload) {
			return Vue.http.get(process.env.API_URL + '/location/countries',
				{ headers: getHeaders() }
			);
		},

		fetchCountries({ state, commit }, payload) {
			return new Promise((resolve, reject) => {
				if (Object.keys(state.countries).length > 0)
					return resolve(state.countries)

				Vue.http.get(`${process.env.API_URL}/location/countries`, {
					headers: getHeaders()
				})
				.then(response => {
					if (response.status == 200) {
						response.body.forEach(country => {
							state.countries[country.id] = country.name;
						});

						return resolve(state.countries);
					}

					return reject(response);
				}, error => commit('logger/error', { message: 'Erro!' }));
			})
		},
	},
	getters: {
		getCountries(state) {
			return state.countries;
		}
	},
	mutations: {
		//
	}
}
