import Vue from 'vue';

import { getHeaders } from '../../';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getPartners (context, payload) {
			return Vue.http.get(process.env.API_URL + '/partners',
				{ headers: getHeaders() }
			);
		},
		addPartner (context, payload) {
			return Vue.http.post(process.env.API_URL + '/partners',
				payload,
				{ headers: getHeaders() }
			)
		},
		getPartner (context, payload) {
			return Vue.http.get(process.env.API_URL + '/partners/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		editPartner (context, payload) {
			return Vue.http.put(process.env.API_URL + '/partners/' + payload.partner.id,
				payload,
				{ headers: getHeaders() }
			);
		},
		deletePartner (context, payload) {
			return Vue.http.delete(process.env.API_URL + '/partners/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		nextPartner(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
