import Vue from 'vue';

import { getHeaders } from '../../';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getPages (context, payload) {
			return Vue.http.get(process.env.API_URL + '/pages',
				{ headers: getHeaders() }
			);
		},
		addPage (context, payload) {
			return Vue.http.post(process.env.API_URL + '/pages',
				payload,
				{ headers: getHeaders() }
			)
		},
		getPage (context, payload) {
			return Vue.http.get(process.env.API_URL + '/pages/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		editPage (context, payload) {
			return Vue.http.put(process.env.API_URL + '/pages/' + payload.page.id,
				payload,
				{ headers: getHeaders() }
			);
		},
		deletePage (context, payload) {
			return Vue.http.delete(process.env.API_URL + '/pages/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		nextPage(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
