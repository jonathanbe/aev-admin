import Vue from 'vue';

import { getHeaders } from '../../';

export default {
	namespaced: true,
	state: {
		//
	},
	actions: {
		getEvents (context, payload) {
			return Vue.http.get(process.env.API_URL + '/events',
				{ headers: getHeaders() }
			);
		},
		addEvent (context, payload) {
			return Vue.http.post(process.env.API_URL + '/events',
				payload,
				{ headers: getHeaders() }
			)
		},
		getEvent (context, payload) {
			return Vue.http.get(process.env.API_URL + '/events/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		editEvent (context, payload) {
			return Vue.http.put(process.env.API_URL + '/events/' + payload.event.id,
				payload,
				{ headers: getHeaders() }
			);
		},
		deleteEvent (context, payload) {
			return Vue.http.delete(process.env.API_URL + '/events/' + payload.id,
				{ headers: getHeaders() }
			);
		},
		nextEvent(context, payload) {
			return Vue.http.get(payload.url);
		}
	},
	getters: {
		//
	},
	mutations: {
		//
	}
}
