var express = require('express'),
	request = require('request'),
	path = require('path'),
	bodyParser = require('body-parser');

let app = express(),
	port = process.env.APP_PORT || 8081;

if (process.env.NODE_ENV == 'production') {
	// Apply gzip compression to js files
	app.get('*.js', function(req, res, next) {
		req.url = req.url + '.gz';
		res.setHeader('Content-Encoding', 'gzip');
		res.setHeader('Content-Type', 'text/javascript');
		next();
	});
}

// Routes
app.use('/', express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Request Access Token
app.post('/authorize', (req, res, next) => {
	var options = {
		method: 'POST',
		url: `${process.env.API_URL}/oauth2/token`,
		proxy: process.env.KONG_PROXY || 'https://kong:8443',
		tunnel: false,
		json: true,
		form: {
			grant_type: req.body.grant_type,
			client_id: process.env.CLIENT_ID,
			client_secret: process.env.CLIENT_SECRET,
		}
	};

	if (options.form.grant_type == 'refresh_token') {
		options.form.refresh_token = req.body.refresh_token;
	}

	if (options.form.grant_type == 'password') {
		options.form.username = req.body.username;
		options.form.password = req.body.password;
		options.form.provision_key = process.env.PROVISION_KEY;
		options.form.authenticated_userid = req.body.user_id;
	}

	return request(options, (err, _, body) => {
		if (err)
			next(err);

		return res.json(body);
	});
});

// Error middlware
app.use((err, req, res, next) => {
	res.status(err.status || 500);
	res.send(err.message);

	console.log(err.stack);
});

// Listen to default port
app.listen(port, (err) => {
	if (err)
		console.error(`Error: ${err}`);

	console.log(`Server started ${port}`);
});

module.exports = app;
